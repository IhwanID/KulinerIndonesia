package id.ihwan.kulinerindonesia;

import java.util.ArrayList;

public class KulinerData {

    public static String[][] data = new String[][]{
            {"Sop Buntut", "", "Sop buntut adalah salah satu masakan populer dalam masakan Indonesia. Terbuat dari potongan ekor sapi yang dibumbui kemudian dibakar atau digoreng dan dimasukkan kedalam kuah kaldu sapi yang agak bening bersama irisan kentang, wortel, tomat, daun bawang, seledry dan taburan bawang goreng. Sop buntut Indonesia dibumbui dengan bawang merah, bawang putih, dan rempah-rempah lokal, seperti merica, pala, dan cengkeh. Variasi yang relatif baru disebut \"Sop Buntut Goreng\" yaitu buntut goreng yang dibumbui disajikan kering, sementara kuah sup kaldunya disajikan dalam mangkuk terpisah."},
            {"Rawon", "Jawa Timur", "\n" +
                    "Nasi rawon empal kisi, Banyuwangi, Jawa Timur\n" +
                    "Rawon adalah masakan Indonesia berupa sup daging berkuah hitam sebagai campuran bumbu khas yang menggunakan kluwek. Rawon, meskipun dikenal sebagai masakan khas Jawa Timur, dikenal pula oleh masyarakat Jawa Tengah sebelah timur (daerah Surakarta).\n" +
                    "\n" +
                    "Daging untuk rawon umumnya adalah daging sapi yang dipotong kecil-kecil, utamanya adalah bagian sandung lamur. Bumbu supnya sangat khas Indonesia, yaitu campuran bawang merah, bawang putih, lengkuas (laos), ketumbar, serai, kunir, lombok, kluwek, garam, serta minyak nabati. Semua bahan ini dihaluskan, lalu ditumis sampai harum. Campuran bumbu ini kemudian dimasukkan dalam kaldu rebusan daging bersama-sama dengan daging. Warna gelap khas rawon berasal dari kluwek. Di luar negeri, rawon disebut sebagai black soup.\n" +
                    "\n" +
                    "Rawon disajikan bersama nasi, dilengkapi dengan tauge kecil, daun bawang, kerupuk udang, daging sapi goreng (empal) dan sambal."},
            {"Opor", "Jawa Timur", "Opor merupakan masakan khas Indonesia yang bahan utamanya adalah ayam dengan kuah dari santan. Resep opor ayam sangatlah kaya akan bumbu dan rempah-rempah, sehingga rasa yang dihasilkan dari masakan ini sangatlah lezat. Ciri utama opor ayam adalah warna masakannya berwarna kuning, warna ini dihasilkan dari salah satu rempah yaitu kunyit."},
            {"Soto", "Jawa Timur", "makanan khas Indonesia seperti sop yang terbuat dari kaldu daging dan sayuran. Daging yang paling sering digunakan adalah daging sapi dan ayam, tetapi ada pula yang menggunakan daging babi atau kambing. Berbagai daerah di Indonesia memiliki soto khas daerahnya masing-masing dengan komposisi yang berbeda-beda, misalnya soto Madura, soto Kediri, soto pemalang , soto Lamongan, soto Jepara, soto Semarang, soto Kudus, soto Betawi, soto Padang, soto Bandung, sroto Sokaraja, soto Banjar, soto Medan, dan coto Makassar. Soto juga diberi nama sesuai isinya, misalnya soto ayam, soto babat, dan soto kambing. Ada pula soto yang dibuat dari daging kaki sapi yang disebut dengan Soto Sekengkel.\n" +
                    "\n" +
                    "Cara penyajian soto berbeda-beda sesuai kekhasan di setiap daerah. Soto biasa dihidangkan dengan nasi, lontong, ketupat, mi, atau bihun disertai berbagai macam lauk, misalnya kerupuk, perkedel, emping, sambal, dan sambal kacang. Ada pula yang menambahkan telur puyuh, sate kerang, jeruk limau, dan koya."},
            {"Pepes", "Jawa Barat", "\n" +
                    "Pepes ikan mas\n" +
                    "Pepes atau Pais merupakan suatu cara khas dari Jawa Barat untuk mengolah bahan makanan (biasanya untuk ikan) dengan bantuan daun pisang untuk membungkus ikan beserta bumbunya. Cara membuatnya adalah bumbu dan rempah dihaluskan dan ditambah daun kemangi, tomat, dan cabai dibalur/dibalut bersama ikan mas yang sudah dibersihkan. Semua lalu dibungkus dengan daun pisang dan disemat dengan 2 buah bambu kecil di setiap ujungnya. Bungkusan ini lalu dibakar (dipepes) di atas api atau bara api dari arang sampai mengering."},
            {"Gudeg", "Jogjakarta", "Gudeg merupakan sayur nangka muda yang dimasak dengan gula merah dan santan. Agar lebih lezat, gudeg biasanya dimakan bersama telur rebus, ayam goreng dan beberapa lauk lainnya."},
            {"Gado-gado", "", "Gado-gado adalah makanan khas Indonesia yang sehat. Kamu bisa lihat dari gambar makanan di atas, isinya adalah sayur-sayuran. Dicampur dengan bumbu kacang super lezat membuat gado-gado menjadi salah satu makanan berisi sayuran yang paling banyak disukai."},
            {"Bakso", "Presiden Ketiga RI", "Makan bakso setiap hari pun tidak masalah. Bakso yang umumnya dikonsumsi adalah bakso sapi dan bakso ikan. Tergantung selera masing-masing, namun menurut kami bakso sapi adalah yang ternikmat."},
            {"Kerak Telur", "Betawi, DKI Jakarta", "Kerak telur adalah campuran antara beras ketan dan telur ayam atau telur bebek yang kemudian dicampur dengan bumbu-bumbu."},
            {"Pempek", "Palembang", "Pempek biasanya dimakan menggunakan bumbunya yang terbuat dari cuka yang dicampur dengan gula. Jika gambar makanan di atas sudah mampu membuatmu lapar, maka bayangkan betapa menggiurkan rasanya."},
            {"Nasi Padang", "", "Nasi Padang adalah makanan yang paling populer di Maritim Asia Tenggara. Bukan cuma disajikan di kota Padang oleh orang-orang Minangkabau, tapi rumah makan Padang sudah populer banget di seluruh Indonesia dan negara tetangga, seperti Malaysia dan Singapura.\n" +
                    "\n" +
                    "Lama-lama, orang lebih suka menyebutnya makanan Padang, bukan Nasi Padang. Karena masakan tersebut bukan hanya berbicara soal nasi, tapi juga seluruh lauk pauknya. Makanan Padang terkenal akan rasanya yang kaya karena terbuat dari santan dan cabai pedas. Karena, di antara tradisi memasak berbagai masakan Indonesia, masakan Minangkabau dan sebagian besar masakan Sumatera menunjukkan pengaruh Timur Tengah dan India yang merupakan hidangan saus kari dengan santan. Dan nggak lupa menggunakan rempah-rempah."},
            {"Rendang", "", "Kelezatan rendang sudah dikenal dunia. Bahan utama rendang adalah daging. Dimasak dengan aneka bumbu khas Indonesia, rasa rendang menjadi tiada duanya. Banyak orang yang langsung ketagihan pada saat pertama kali mereka mencicipi rendang."},
            {"Sate", "", " Makanan yang menggunakan tusuk bambu dan menghasilkan bau harum saat dibakar ini adalah makanan kesukaan banyak orang Indonesia. Jenis sate ada bermacam-macam, seperti sate ayam, babi dan kambing."},
            {"Nasi Goreng", "", "Nasi goreng adalah sebuah hidangan nasi yang telah digoreng dalam sebuah wajan atau penggorengan dan biasanya dicampur dengan bahan-bahan lainnya seperti telur, sayur-sayuran, makanan laut, atau daging. Makanan tersebut seringkali disantap sendiri atau disertai dengan hidangan lainnya. Nasi goreng adalah komponen populer dari masakan Asia Timur, Tenggara dan Selatan pada wilayah tertentu. Sebagai hidangan buatan rumah, nasi goreng biasanya dibuat dengan bahan-bahan yang tersisa dari hidangan lainnya, yang berujung pada ragam yang tak terbatas. "},
            {"Bika Ambon", "Ambon", "Bika ambon terbuat dari bahan-bahan utama pembuat kue, seperti telur, gula dan santan."}
    };

    public static Integer[] gambar = new Integer[]{
      R.drawable.nasi_goreng,
      R.drawable.rawon,
      R.drawable.opor,
      R.drawable.soto,
      R.drawable.pepes,
      R.drawable.gudeg,
      R.drawable.gado_gado,
      R.drawable.bakso,
      R.drawable.kerak_telor,
      R.drawable.pempek,
      R.drawable.pempek,
      R.drawable.nasi_goreng,
      R.drawable.rendang,
      R.drawable.sate,
      R.drawable.nasi_goreng,
      R.drawable.bika_ambon,
    };

    public static ArrayList<Kuliner> getListData(){
        Kuliner kuliner = null;
        ArrayList<Kuliner> list = new ArrayList<>();
        for (int i = 0; i <data.length; i++) {
            kuliner = new Kuliner();
            kuliner.setNama(data[i][0]);
            kuliner.setAsal(data[i][1]);
            kuliner.setDeskripsi(data[i][2]);
            kuliner.setPhoto(gambar[i]);
            list.add(kuliner);
        }

        return list;
    }
}
