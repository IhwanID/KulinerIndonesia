package id.ihwan.kulinerindonesia;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvCategory;
    private ArrayList<Kuliner> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvCategory = findViewById(R.id.rv_category);
        rvCategory.setHasFixedSize(true);

        list = new ArrayList<>();
        list.addAll(KulinerData.getListData());

        showRecyclerCardView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Tentang Aplikasi")
                        .setMessage("\nKuliner Indonesia V.1.0 \n \nDibuat dengan ❤️ oleh Ihwan ID")
                .show();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void showRecyclerCardView(){
        rvCategory.setLayoutManager(new LinearLayoutManager(this));
        CardViewKulinerAdapter cardViewKulinerAdapter = new CardViewKulinerAdapter(this);
        cardViewKulinerAdapter.setListKuliner(list);
        rvCategory.setAdapter(cardViewKulinerAdapter);

        ItemClickSupport.addTo(rvCategory).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                showSelectedKuliner(list.get(position));
            }
        });
    }

    private void showSelectedKuliner(Kuliner kuliner){
        Toast.makeText(this, kuliner.getNama()+" Enak Banget Loh", Toast.LENGTH_SHORT).show();
    }


}
