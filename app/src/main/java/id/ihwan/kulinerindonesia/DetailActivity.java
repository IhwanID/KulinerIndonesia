package id.ihwan.kulinerindonesia;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DetailActivity extends AppCompatActivity {

    TextView namaMakanan, asalMakanan, deskripsiMakanan;
    ImageView gambarMakanan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        namaMakanan = findViewById(R.id.detail_nama_makanan);
        asalMakanan = findViewById(R.id.detail_asal_makanan);
        deskripsiMakanan = findViewById(R.id.detail_deskripsi_makanan);
        gambarMakanan = findViewById(R.id.detail_gambar);

        String nama = getIntent().getStringExtra("NAMA");
        String asal = getIntent().getStringExtra("ASAL");
        String deskripsi = getIntent().getStringExtra("DESKRIPSI");
        int gambar = getIntent().getIntExtra("GAMBAR", 0);

        namaMakanan.setText(nama);
        asalMakanan.setText(asal);
        deskripsiMakanan.setText(deskripsi);
        gambarMakanan.setImageResource(gambar);
    }
}
