package id.ihwan.kulinerindonesia;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CardViewKulinerAdapter extends RecyclerView.Adapter<CardViewKulinerAdapter.CardViewViewHolder>  {

    private ArrayList<Kuliner> listKuliner;
    private Context context;


    public CardViewKulinerAdapter(Context context) {
        this.context = context;
    }

    public ArrayList<Kuliner> getListKuliner() {
        return listKuliner;
    }

    public void setListKuliner(ArrayList<Kuliner> listKuliner) {
        this.listKuliner = listKuliner;
    }

    @NonNull
    @Override
    public CardViewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cardview_kuliner, parent, false);
        CardViewViewHolder viewHolder = new CardViewViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewViewHolder holder, int position) {
        Kuliner k = getListKuliner().get(position);

        holder.gambarMakanan.setImageResource(k.getPhoto());

        holder.namaMakanan.setText(k.getNama());
        holder.asalMakanan.setText(k.getAsal());

        holder.btnDetail.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {

            @Override
            public void onItemClicked(View view, int position) {
                Intent i = new Intent(context, DetailActivity.class);
                i.putExtra("NAMA", getListKuliner().get(position).getNama());
                i.putExtra("ASAL", getListKuliner().get(position).getAsal());
                i.putExtra("DESKRIPSI", getListKuliner().get(position).getDeskripsi());
                i.putExtra("GAMBAR", getListKuliner().get(position).getPhoto());
                view.getContext().startActivity(i);
            }
        }));

    }

    @Override
    public int getItemCount() {
        return getListKuliner().size();
    }

    public class CardViewViewHolder extends RecyclerView.ViewHolder{
        ImageView gambarMakanan;
        TextView namaMakanan, asalMakanan;

        Button btnDetail;
        public CardViewViewHolder(View itemView) {
            super(itemView);
            gambarMakanan = itemView.findViewById(R.id.gambar_makanan);
            namaMakanan = itemView.findViewById(R.id.nama_makanan);
            asalMakanan = itemView.findViewById(R.id.asal_makanan);
            btnDetail = itemView.findViewById(R.id.btn_detail);;
        }
    }
}
